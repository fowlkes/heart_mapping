%
% heuristics for cleaning up mistakes in a given trace
%
% charless fowlkes (c) 2020
%


function trace = clean_trace(trace,vis)

  if (nargin==1)
    vis = 0
  end

  nnodes = size(trace.x,1);

  % drop long edges
  nonroot = find(trace.parent>0);
  dx = trace.x(nonroot,1)-trace.x(trace.parent(nonroot),1);
  dy = trace.x(nonroot,2)-trace.x(trace.parent(nonroot),2);
  dz = trace.zscale*(trace.x(nonroot,3)-trace.x(trace.parent(nonroot),3));
  d = zeros(nnodes,1);
  d(nonroot) = (dx.^2+dy.^2+dz.^2).^0.5;
  bad = find((d>=20) & (trace.radius<5));
  trace.parent(bad) = -1;
  if (vis)
    figure(vis);
    clf;
    rendertrace2d_select(trace,bad)
    title('dropped long edges');
    drawnow;
    pause(1);
  end

  % remove junctions by dropping highest discrepancy children
  parentlist = trace.parent(trace.parent>0);
  childcount = sparse(parentlist,ones(length(parentlist),1),1,nnodes,1);
  junctions = find(childcount>1);
 
  if (vis)
    figure(vis);
    clf;
    rendertrace2d_select(trace,junctions)
    title('junctions removed');
    drawnow;
    pause(1);
  end

  for i = 1:length(junctions)
    children = find(trace.parent==junctions(i));
    dr = abs(trace.radius(junctions(i))-trace.radius(children));
    [~,ind] = min(dr);
    trace.parent(children([1:(ind-1) (ind+1):length(children)])) = -1;
    fprintf('\r %d ',i);
  end

  % now we can create child list
  trace.child = -1*ones(nnodes,1);
  hasparents = find(trace.parent>0);
  trace.child(trace.parent(hasparents)) = hasparents;

  % remove isolated nodes 
  isolated = find((trace.parent==-1) & (trace.child==-1));
  if (vis)
    figure(vis);
    clf;
    rendertrace2d_select(trace,isolated)
    title('remove isolated nodes');
    drawnow;
    pause(1);
  end
  trace = dropnodes(trace,isolated);

  % remove pairs
  pair1 = find((trace.parent==-1) & (trace.child~=-1));
  pair1 = pair1(find(trace.child(trace.child(pair1)) == -1));
  %pair2 = find((trace.child==-1) & (trace.parent~= -1));
  %pair2 = pair2(find(trace.parent(trace.parent(pair2)) == -1));
  pair = [pair1; trace.child(pair1)];
  if (vis)
    figure(vis);
    clf;
    rendertrace2d_select(trace,pair)
    title('remove doublets');
    drawnow;
    pause(1);
  end
  trace = dropnodes(trace,pair);

  % now everything is a triple or larger

  % remove endpoint swells
  for i = 1:50
    leaf = find((trace.child==-1) & (trace.parent>0));
    dr1 = abs(trace.radius(leaf)-trace.radius(trace.parent(leaf))) ./ trace.radius(leaf);
    dr2 = abs(trace.radius(trace.parent(leaf))-trace.radius(trace.parent(trace.parent(leaf)))) ./ trace.radius(trace.parent(leaf));

    root = find((trace.parent==-1) & (trace.child>0));
    dr3 = abs(trace.radius(root)-trace.radius(trace.child(root))) ./ trace.radius(root);
    dr4 = abs(trace.radius(trace.child(root))-trace.radius(trace.child(trace.child(root)))) ./ trace.radius(trace.child(root));

    thresh = 0.4;
    bad = zeros(nnodes,1);
    bad(leaf) = (dr1>thresh);
    bad(trace.parent(leaf)) = (dr2>thresh);
    bad(root) = (dr3>thresh);
    bad(trace.child(root)) = (dr4>thresh);
    badind = find(bad);

    if (vis)
      figure(vis);
      clf;
      rendertrace2d_select(trace,badind)
      title(sprintf('remove swells iter = %d',i));
      drawnow;
      pause(1);
    end
    if (length(badind)==0)
      break;
    end
    trace = dropnodes(trace,badind);

    % remove isolated nodes 
    isolated = find((trace.parent==-1) & (trace.child==-1));
    trace = dropnodes(trace,isolated);

    % remove pairs
    pair1 = find((trace.parent==-1) & (trace.child~=-1));
    pair1 = pair1(find(trace.child(trace.child(pair1)) == -1));
    pair = [pair1; trace.child(pair1)];
    trace = dropnodes(trace,pair);
  end

  if (vis)
    figure(vis);
    clf;
    rendertrace2d_radius(trace)
    title(sprintf('original radius'));
    drawnow;
    pause(1);
  end

  % find places where successive nodes have very different radii 
  nonroot = find(trace.parent>0);
  dr = abs(trace.radius(nonroot)-trace.radius(trace.parent(nonroot))) ./ trace.radius(nonroot);

  % where there are jumps in trace.radius along a trace, set the
  % larger trace.radius to the value of its smaller neighbor
  thresh = 1.0;
  for i = 1:4
    mr = min(trace.radius(nonroot(dr>thresh)),trace.radius(trace.parent(nonroot(dr>thresh))));
    trace.radius(nonroot(dr>thresh)) = mr;
    trace.radius(trace.parent(nonroot(dr>thresh))) = mr;
    dr = abs(trace.radius(nonroot)-trace.radius(trace.parent(nonroot))) ./ trace.radius(nonroot);
  end

  if (vis)
    figure(vis);
    clf;
    rendertrace2d_radius(trace)
    title(sprintf('smoothed radius'));
    drawnow;
    pause(1);
  end

