function rendertrace3d_select(trace,bad)


rrange = [linspace(0.4,2,5) linspace(2,8,5)];
rmap = 2*rrange;
rrange = [rrange 100];
rmap = [0.3 rmap];
cmap = max(jet(length(rrange))-0.1,0);

% visualization of fiber radius
for s = length(rrange):-1:2
  ind = find((trace.radius>=rrange(s-1)) & (trace.radius<rrange(s)));
  ind1 = intersect(ind,bad);
  ind2 = setdiff(ind,bad);
  plot3(trace.x(ind1,1),trace.x(ind1,2),trace.zscale*trace.x(ind1,3),'r.','MarkerSize',rmap(s));
  plot3(trace.x(ind2,1),trace.x(ind2,2),trace.zscale*trace.x(ind2,3),'b.','MarkerSize',rmap(s));
  hold on;
end
axis image; axis vis3d; grid on;

