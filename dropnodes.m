function trace = dropnodes(dirtytrace,bad)

trace = dirtytrace;
trace.x(bad,:) =0;
trace.radius(bad) = -1;
trace.parent(bad) = -1;
children = find(ismember(trace.parent,bad));
trace.parent(children) = -1;

if isfield(trace,'child')
  trace.child(bad) = -1;
end

%todo:  renumber to remove bad nodes from swc
