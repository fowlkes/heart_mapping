function [m] = clone_ome_metadata(metadata,orig_omemeta)

m = metadata;
om = orig_omemeta;


m.getRoot().addInstrument(ome.xml.model.Instrument(om.getRoot().getInstrument(0)));
m.getRoot().getImage(0).linkInstrument(m.getRoot().getInstrument(0));
m.setExperimenterID(om.getExperimenterID(0),0);
m.setExperimenterUserName(om.getExperimenterUserName(0),0);

for i = 0:m.getImageCount()-1
  %m.setImageAcquisitionDate(om.getImageAcquisitionDate(i),i);
  %m.setImageDescription(om.getImageDescription(i),i);
  %m.setImageExperimenterRef(om.getImageExperimenterRef(i),i);
  %m.setImageInstrumentRef(om.getImageInstrumentRef(i),i);
  m.setObjectiveSettingsID(om.getObjectiveSettingsID(i),i);
  m.setObjectiveSettingsMedium(om.getObjectiveSettingsMedium(i),i);
  m.setObjectiveSettingsRefractiveIndex(om.getObjectiveSettingsRefractiveIndex(i),i);

  for j = 0:m.getChannelCount(i)-1
    m.setChannelAcquisitionMode(om.getChannelAcquisitionMode(i,j),i,j);
    m.setChannelColor(om.getChannelColor(i,j),i,j);
    m.setChannelEmissionWavelength(om.getChannelEmissionWavelength(i,j),i,j);
    m.setChannelExcitationWavelength(om.getChannelExcitationWavelength(i,j),i,j);
    m.setChannelFluor(om.getChannelFluor(i,j),i,j);
    m.setChannelID(om.getChannelID(i,j),i,j);
    m.setChannelIlluminationType(om.getChannelIlluminationType(i,j),i,j);
    m.setChannelName(om.getChannelName(i,j),i,j);
    %m.setChannelSamplesPerPixel(om.getChannelSamplesPerPixel(i,j),i,j);
    m.setDetectorSettingsBinning(om.getDetectorSettingsBinning(i,j),i,j);
    m.setDetectorSettingsID(om.getDetectorSettingsID(i,j),i,j);
  end
end



% copy the instrument / detector info
%m.getRoot().setInstrument(0,ome.xml.model.Instrument(om.getRoot().getInstrument(0)));
%m.getRoot().getInstrument(0).addObjective(ome.xml.model.Objective(om.getRoot().getInstrument(0).getObjective(0)));

%for i = 0:om.getDetectorCount(0)-1
%  m.getRoot().getInstrument(0).addDetector(ome.xml.model.Detector( om.getRoot().getInstrument(0).getDetector(i)));
%end

%for i = 0:om.getDetectorCount(0)-1
%  m.setDetectorAmplificationGain(om.getDetectorAmplificationGain(0,i),0,i);
%  m.setDetectorGain(om.getDetectorGain(0,i),0,i);
%  m.setDetectorID(om.getDetectorID(0,i),0,i);
%  m.setDetectorType(om.getDetectorType(0,i),0,i);
%  m.setDetectorZoom(om.getDetectorZoom(0,i),0,i);
%end

%for i = 0:om.getObjectiveCount(0)-1
%  m.setObjectiveID(m.getObjectiveID(0,i),0,i);
%  m.setObjectiveImmersion(m.getObjectiveImmersion(0,i),0,i);
%  m.setObjectiveLensNA(m.getObjectiveLensNA(0,i),0,i);
%  m.setObjectiveModel(m.getObjectiveModel(0,i),0,i);
%  m.setObjectiveNominalMagnification(m.getObjectiveNominalMagnification(0,i),0,i);
%end

% use image count for new metadata (e.g. in case we stitched tiles)
%for i = 0:m.getImageCount()-1
%  m.setImageID(om.getImageID(i),i);
%  m.setImageName(om.getImageName(i),i);
%
%  m.setImageAcquisitionDate(om.getImageAcquisitionDate(i),i);
%  m.setImageDescription(om.getImageDescription(i),i);
%
%  m.setImageExperimenterRef(om.getImageExperimenterRef(i),i);
%  m.setImageInstrumentRef(om.getImageInstrumentRef(i),i);
%  m.setObjectiveSettingsID(om.getObjectiveSettingsID(i),i);
%  m.setObjectiveSettingsMedium(om.getObjectiveSettingsMedium(i),i);
%  m.setObjectiveSettingsRefractiveIndex(om.getObjectiveSettingsRefractiveIndex(i),i);
%  m.setPixelsBigEndian(om.getPixelsBigEndian(i),i);
%  m.setPixelsDimensionOrder(om.getPixelsDimensionOrder(i),i);
%  m.setPixelsID(om.getPixelsID(i),i);
%  m.setPixelsInterleaved(om.getPixelsInterleaved(i),i);
%  m.setPixelsPhysicalSizeX(om.getPixelsPhysicalSizeX(i),i);
%  m.setPixelsPhysicalSizeY(om.getPixelsPhysicalSizeY(i),i);
%  m.setPixelsPhysicalSizeZ(om.getPixelsPhysicalSizeZ(i),i);
%  m.setPixelsSignificantBits(om.getPixelsSignificantBits(i),i);
%  m.setPixelsSizeC(om.getPixelsSizeC(i),i);
%  m.setPixelsSizeT(om.getPixelsSizeT(i),i);
%  m.setPixelsSizeX(om.getPixelsSizeX(i),i);
%  m.setPixelsSizeY(om.getPixelsSizeY(i),i);
%  m.setPixelsSizeZ(om.getPixelsSizeZ(i),i);
%  m.setPixelsType(om.getPixelsType(i),i);
%
%  for j = 0:m.getChannelCount(i)-1
%    m.setChannelAcquisitionMode(om.getChannelAcquisitionMode(i,j),i,j);
%    m.setChannelColor(om.getChannelColor(i,j),i,j);
%    m.setChannelEmissionWavelength(om.getChannelEmissionWavelength(i,j),i,j);
%    m.setChannelExcitationWavelength(om.getChannelExcitationWavelength(i,j),i,j);
%    m.setChannelFluor(om.getChannelFluor(i,j),i,j);
%    m.setChannelID(om.getChannelID(i,j),i,j);
%    m.setChannelIlluminationType(om.getChannelIlluminationType(i,j),i,j);
%    m.setChannelName(om.getChannelName(i,j),i,j);
%    m.setChannelSamplesPerPixel(om.getChannelSamplesPerPixel(i,j),i,j);
%    m.setDetectorSettingsBinning(om.getDetectorSettingsBinning(i,j),i,j);
%    m.setDetectorSettingsID(om.getDetectorSettingsID(i,j),i,j);
%  end
%end
%
