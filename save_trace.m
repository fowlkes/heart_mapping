%
% save a trace as a .swc file
%
% charless fowlkes (c) 2020

function save_trace(trace,filename)

fileID = fopen(filename,'w');
if (fileID>0)
  id = 1:length(trace.parent); 
  id = id';
  type = ones(length(trace.parent),1);
  swcblock = [id type trace.x(:,1) trace.x(:,2) trace.x(:,3) trace.radius trace.parent];
  %Colums are: id type x y z radius parentid
  fprintf(fileID, '%d %d %3.3f %3.3f %3.3f %3.3f %d\n',swcblock');
  fclose(fileID);
else
  error(['error opening file:' filename]);
end

