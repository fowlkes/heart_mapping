
function LEG = ori_legend(res)

[xx,yy] = meshgrid(linspace(-1,1,res),linspace(-1,1,res));
zz = xx.^2+yy.^2;
[dx,dy] = gradient(zz);
%theta = atan2(dy,dx);
%dx = sin(2*theta);
%dy = cos(2*theta);
%omap = mod(atan2(dx,dy),2*pi)/2;
omap = atan(dy./dx)+(pi/2);

oriquant = int16(floor(255*(omap/pi))+1);
cmap = hsv(256);
LEG = zeros(prod(size(dx)),3);
LEG(:,1) = cmap(oriquant(:),1).*(zz(:)>0.01).*(zz(:)<1);
LEG(:,2) = cmap(oriquant(:),2).*(zz(:)>0.01).*(zz(:)<1);
LEG(:,3) = cmap(oriquant(:),3).*(zz(:)>0.01).*(zz(:)<1);
LEG = reshape(LEG,[size(dx,1) size(dx,2) 3]);

figure(2); imagesc(LEG)



figure(1); clf;
t= 0:0.01:2*pi;
x = 20*cos(t);
y = 20*sin(t);
dx = x(1:end-1)-x(2:end);
dy = y(1:end-1)-y(2:end);
theta = atan(dy./dx);
thetarange = linspace(-pi/2,pi/2,10);
cmap = 0.9*hsv(length(thetarange));
for t = 2:length(thetarange)
  ind = find((theta>=thetarange(t-1)) & (theta<thetarange(t)));
  plot(x(ind),y(ind),'.','MarkerSize',10,'Color',cmap(t,:));
  hold on;
end
axis image; grid on;



set(gcf,'color',[0 0 0]);
whitebg

figure(2)
set(gcf,'color',[0 0 0]);
axis off;
print('ori_colorbar2.pdf','-dpdf','-bestfit','-r900') 

