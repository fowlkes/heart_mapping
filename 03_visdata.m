%
% visualize example data (using the provided swc file)
%

trace = load_trace('heart4_dorsal_clean.swc',4.39);

figure(3); clf;
whitebg;
rendertrace2d_radius(trace); axis ij;
axis off;
set(gcf,'color',[0 0 0]);
set(gcf,'InvertHardcopy','off');
%print('clean_final1.pdf','-dpdf','-bestfit','-r900') 

figure(4); clf;
whitebg;
rendertrace2d_ori(trace); axis ij;
axis off;
set(gcf,'color',[0 0 0]);
set(gcf,'InvertHardcopy','off');
%print('clean_final_ori.pdf','-dpdf','-bestfit','-r900') 

% orientation legend
% figure(5); clf;
%imagesc(ori_legend(400));
%axis xy; axis off
%set(gcf,'color',[0 0 0]);
%print('ori_colorbar.pdf','-dpdf','-bestfit','-r900') 

