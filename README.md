This repository contains example code used to perform data processing described
in the following publication:

  ["Identification of peripheral neural circuits that regulate heart rate using
  optogenetic and viral vector strategies", P. Rajendran et al,  Nature
  Communications 10:1944 (April 2019)](https://www.nature.com/articles/s41467-019-09770-1)

To utilize it for your own purposes will likely require making some
modifications.


### Dependencies


*MATLAB*

*BioFormats tools*

  Used for performing conversions between different image file formats
  and extracting tiles.

  available here: 
    
  [https://docs.openmicroscopy.org/bio-formats/6.5.0/users/comlinetools/index.html](https://docs.openmicroscopy.org/bio-formats/6.5.0/users/comlinetools/index.html)

*neuTube*

  Used for performing automated tracing of individual tiles.  We utilize a
  modified version which fixes a few bugs and provides commandline flags for
  tracing with a fixed threshold. This version has only been built and tested
  on Linux (Ubuntu):
  
  [https://bitbucket.org/fowlkes/neutube2/src/neutube/](https://bitbucket.org/fowlkes/neutube2/src/neutube/)

  You can find the original neuTube here which also provides precompiled 
  executables for several platforms (but without the fixed thresholding
  modification):

  [https://www.neutracing.com/](https://www.neutracing.com/)



### Pipeline Overview

To perform tracing on large image stacks using neuTube,
it is necessary to preprocess your image in order to 
break it into single channel tiles.  neuTube can load
tiff stacks but doesn't load other tif formats (e.g., czi,
ome.tiff etc.).  We utilize the bioformats tools to process
images from the command line. 

**01_tracingscript:** contains code that generates a bunch of
scripts that in turn handle preprocessing and tracing.  This
will likely require substantial modification to be adapted
to other datasets.

However, the overall process is straightforward and involves
3 steps (which can also be carried out manually rather than
via scripting)
    
    % extract a single spectral channel 0
    bfconvert -channel 0 mystitchedimage.czi c0.ome.tif

    % break large image in to 2048x2048 tiles
    bfconvert -tilex 2048 -tiley 2048 c0.ome.tif tiles/tile_0_%x_%y.tif

    % run neuTube on each individual tile
    neuTube --command --level 2 --thresh 6000 -o tile_0_0_0.swc --trace tile_0_0_0.tif
    neuTube --command --level 2 --thresh 6000 -o tile_0_0_1.swc --trace tile_0_0_1.tif
    .
    .
    .

By default, neuTube uses an adaptive threshold. This is problematic
since it results in each tile being processed differently.  The 
--thresh command line switch allows you to set a fix threshold instead.

The tracing threshold can be estimated by first running neuTube using 
the commandline flag "--computethresh" :
    
    neuTube --command --level 2 --computethresh -o tmp.swc --trace myimage.tif

where myimage.tif is some representative tile. This will cause neuTube to
process the image to calculate the threshold and then print the value out to
the console (without performing tracing).  You may need to experiment with the
appropriate threshold to find the best tradeoff of between missing fibers and
false positives.




**02_stitching:**  contains code that loads in the swc files for 
each individual tile and combines them together into a 
single large trace.  It also performs some heuristic cleaning
of the traces to eliminate some common errors in the automatic
tracing.  The thresholds and heuristics may need to be adjusted
to work well on other datasets.


**03_visdata:**  loads in an assembled swc file and produces a couple
visualizations (e.g., fiber orientation and diameter).  You can
use this to visualize the swc file contained in this repo to 
recreate a version of the paper figure.


