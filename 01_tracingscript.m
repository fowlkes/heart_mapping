
%
% Generate pre-processing and tracing scripts.  Once all 
% the scripts have been generated, you can run all of them
% via:
%
%%% generate preprocessing and threshold queryies
%
% chmod u+x */preprocess.sh
% for i in */preprocess.sh; do $i; done
% chmod u+x */stiles/querythresh.sh
% for i in */stiles/querythresh.sh; do $i; done
%
%%%% analyze thresholds to choose a fixed threshold
%%%% for tracing all the tiles.
%
%%%% generate the tracing scripts
%
% chmod u+x */stiles/trace.sh
% for i in */stiles/trace.sh; do $i; done
%
%
%%%% alternately you can use paralell (for neuTube steps)
% 
% cat querythresh.jobs | parallel
% cat trace.jobs | parallel -j 10 --joblog trace.log
%
%
% charless fowlkes (c) 2020
%
basedir = '/mnt/data3/fowlkes/SPARC_HEART/opmap';
bfconvert_o = '/home/fowlkes/bin/bftools_old/bfconvert';
showinf = '/home/fowlkes/bin/bftools/showinf';
neutube = '/home/fowlkes/proj/neutube/NeuTu/neurolabi/build/neuTube';

%
% pre-processing into tiles
%
% TODO: this leaves tile files which have sizeZ and sizeT swapped?
%  This isn't a problem for tracing but is a mystery to be solved
%
DD = dir([basedir '/op*']);
for j = 1:length(DD)
  subdir = DD(j).name;
  basefile = subdir;

  czifile = sprintf('%s.czi',basefile);
  c0file = sprintf('%s_c0.ome.tif',basefile);
  c1file = sprintf('%s_c1.ome.tif',basefile);
  c2file = sprintf('%s_c2.ome.tif',basefile);
  sczifile = sprintf('%s_stitch.czi',basefile);
  sc0file = sprintf('%s_stitch_c0.ome.tif',basefile);
  sc1file = sprintf('%s_stitch_c1.ome.tif',basefile);
  sc2file = sprintf('%s_stitch_c2.ome.tif',basefile);
  tile0file = sprintf('%s_tile_0_%%x_%%y.tif',basefile);
  tile1file = sprintf('%s_tile_1_%%x_%%y.tif',basefile);
  tile2file = sprintf('%s_tile_2_%%x_%%y.tif',basefile);

  scriptfile = sprintf('%s/%s/preprocess.sh',basedir,subdir);
  fid = fopen(scriptfile,'w');
  fprintf(fid,'#!/bin/bash\n');
  fprintf(fid,'## preprocessing czi data %s/%s\n',basedir,subdir);
  fprintf(fid,'set -x\n');
  fprintf(fid,'set +e\n\n\n');
  fprintf(fid,'BFCONVERT=%s\n\n',bfconvert_o);
  fprintf(fid,'SHOWINF=%s\n\n',showinf);
  fprintf(fid,'mkdir -p %s/%s/stiles\n',basedir,subdir);
  fprintf(fid,'cd %s/%s/\n',basedir,subdir);
  fprintf(fid,'MAXZ="$("$SHOWINF" -nopix %s | grep "SizeZ =" | cut -d= -f2)";\n',sczifile);
  fprintf(fid,'$BFCONVERT -channel 0 %s %s\n',sczifile,sc0file);
  fprintf(fid,'$BFCONVERT -channel 1 %s %s\n',sczifile,sc1file);
  fprintf(fid,'$BFCONVERT -channel 2 %s %s\n',sczifile,sc2file);
  fprintf(fid,'echo "Extracting slices 200-" $MAXZ\n');
  fprintf(fid,'$BFCONVERT -range 200 $MAXZ -tilex 2048 -tiley 2048 %s stiles/%s\n',sc0file,tile0file);
  fprintf(fid,'$BFCONVERT -range 200 $MAXZ -tilex 2048 -tiley 2048 %s stiles/%s\n',sc1file,tile1file);
  fprintf(fid,'$BFCONVERT -range 200 $MAXZ -tilex 2048 -tiley 2048 %s stiles/%s\n',sc2file,tile2file);
  fprintf(fid,'for i in stiles/*_tile*.tif; do $BFCONVERT $i ${i/.tif/.a.tif}; done\n');
  fprintf(fid,'for i in stiles/*_tile*a.tif; do mv -f $i ${i/.a.tif/.tif}; done');
  fclose(fid);
end

%
% compute thresholds
%
DD = dir([basedir '/op*']);
for i = 1:length(DD)
  subdir = DD(i).name;
  basefile = subdir;
  datadir= sprintf('%s/%s/stiles',basedir,subdir);
  edatadir= sprintf('%s/%s/stiles',basedir,subdir);

  scriptfile = [datadir '/querythresh.sh'];
  fid = fopen(scriptfile,'w');
  fprintf(fid,'#!/bin/bash\n');
  fprintf(fid,'## checking auto thresholds in %s \n',datadir);
  fprintf(fid,'set -x\n');
  fprintf(fid,'set +e\n\n\n');
  fprintf(fid,'NEUTUBE=%s\n\n',neutube);
  fprintf(fid,'DATADIR=%s\n\n',edatadir);

  % only process channel 0 and 2 since channel 1 is just autofluorescence
  DDD = dir([datadir '/*tile_0*.tif']);
  for j = 1:length(DDD)
    imfile = DDD(j).name;
    threshfile =  [DDD(j).name(1:end-4) '.thresh'];
    fprintf(fid,'$NEUTUBE --command --level 2 --computethresh -o tmp.swc --trace $DATADIR/%s &> $DATADIR/%s\n',imfile,threshfile);
    fprintf(fid,'\n');
  end
  DDD = dir([datadir '/*tile_2*.tif']);
  for j = 1:length(DDD)
    imfile = DDD(j).name;
    threshfile =  [DDD(j).name(1:end-4) '.thresh'];
    fprintf(fid,'$NEUTUBE --command --level 2 --computethresh -o tmp.swc --trace $DATADIR/%s &> $DATADIR/%s\n',imfile,threshfile);
    fprintf(fid,'\n');
  end
  fclose(fid);
end

%
% parallel job queue version
%
jobfile = [basedir '/querythresh.jobs'];
fid = fopen(jobfile,'w');
DD = dir([basedir '/op*']);
for i = 1:length(DD)
  subdir = DD(i).name;
  basefile = subdir;
  datadir= sprintf('%s/%s/stiles',basedir,subdir);
  edatadir= sprintf('%s/%s/stiles',basedir,subdir);

  % only process channel 0 and 2 since channel 1 is just autofluorescence
  DDD = dir([datadir '/*tile_0*.tif']);
  for j = 1:length(DDD)
    imfile = [edatadir '/' DDD(j).name];
    threshfile =  [edatadir '/' DDD(j).name(1:end-4) '.thresh'];
    fprintf(fid,'%s --command --level 2 --computethresh -o tmp.swc --trace %s &> %s\n',neutube,imfile,threshfile);
  end
  DDD = dir([datadir '/*tile_2*.tif']);
  for j = 1:length(DDD)
    imfile = [edatadir '/' DDD(j).name];
    threshfile =  [edatadir '/' DDD(j).name(1:end-4) '.thresh'];
    fprintf(fid,'%s --command --level 2 --computethresh -o tmp.swc --trace %s &> %s\n',neutube,imfile,threshfile);
  end
end
fclose(fid);


%
% constant threshold tracing scripts
%

thresh0 = 6000  %channel 0 threshold
thresh2 = 6000  %channel 2 threshold

DD = dir([basedir '/op*']);
for i = 1:length(DD)
  subdir = DD(i).name;
  basefile = subdir;
  datadir = sprintf('%s/%s/stiles',basedir,subdir);
  edatadir = sprintf('%s/%s/stiles',basedir,subdir);

  scriptfile = [datadir '/trace.sh'];
  fid = fopen(scriptfile,'w');
  fprintf(fid,'#!/bin/bash\n');
  fprintf(fid,'## processing %s with thresholds %d,%d \n',datadir,thresh0,thresh2);
  fprintf(fid,'set -x\n');
  fprintf(fid,'set +e\n\n\n');
  fprintf(fid,'NEUTUBE=%s\n\n',neutube);
  fprintf(fid,'DATADIR=%s\n\n',edatadir);

  DDD = dir([datadir '/*tile_0*.tif']);
  for j = 1:length(DDD)
    imfile = DDD(j).name;
    swcfile =  [DDD(j).name(1:end-4) '.swc'];
    logfile =  [DDD(j).name(1:end-4) '.log'];
    fprintf(fid,'$NEUTUBE --command --level 2 --thresh %d -o $DATADIR/%s --trace $DATADIR/%s &> $DATADIR/%s\n',thresh0,swcfile,imfile,logfile);
  end
  DDD = dir([datadir '/*tile_2*.tif']);
  for j = 1:length(DDD)
    imfile = DDD(j).name;
    swcfile =  [DDD(j).name(1:end-4) '.swc'];
    logfile =  [DDD(j).name(1:end-4) '.log'];
    fprintf(fid,'$NEUTUBE --command --level 2 --thresh %d -o $DATADIR/%s --trace $DATADIR/%s &> $DATADIR/%s\n',thresh2,swcfile,imfile,logfile);
  end
  fclose(fid);
end

%
% parallel job queue version
%
thresh0 = 6000  %channel 0 threshold
thresh2 = 6000  %channel 2 threshold

jobfile = [basedir '/trace.jobs'];
fid = fopen(jobfile,'w');
DD = dir([basedir '/op*']);
for i = 1:length(DD)
  subdir = DD(i).name;
  basefile = subdir;
  datadir= sprintf('%s/%s/stiles',basedir,subdir);
  edatadir= sprintf('%s/%s/stiles',basedir,subdir);

  % only process channel 0 and 2 since channel 1 is just autofluorescence
  DDD = dir([datadir '/*tile_0*.tif']);
  for j = 1:length(DDD)
    imfile = [edatadir '/' DDD(j).name];
    swcfile =  [edatadir '/' DDD(j).name(1:end-4) '.swc'];
    logfile =  [edatadir '/' DDD(j).name(1:end-4) '.log'];
    fprintf(fid,'%s --command --level 2 --thresh %d -o %s --trace %s &> %s\n',neutube,thresh0,swcfile,imfile,logfile);
  end
  DDD = dir([datadir '/*tile_2*.tif']);
  for j = 1:length(DDD)
    imfile = [edatadir '/' DDD(j).name];
    swcfile =  [edatadir '/' DDD(j).name(1:end-4) '.swc'];
    logfile =  [edatadir '/' DDD(j).name(1:end-4) '.log'];
    fprintf(fid,'%s --command --level 2 --thresh %d -o %s --trace %s &> %s\n',neutube,thresh0,swcfile,imfile,logfile);
  end
end
fclose(fid);


%
% generate additional visualizations 
%
addpath bfmatlab

%
% generate max projections of each tile
%
DD = dir([basedir '/op*']);
for i = 1:length(DD)
  subdir = DD(i).name;
  basefile = subdir;
  datadir= sprintf('%s/%s/stiles',basedir,subdir);
  edatadir= sprintf('%s/%s/stiles',basedir,subdir);

  % only process channel 0 and 2 since channel 1 is just autofluorescence
  for c = [0,2]
    DDD = dir([datadir '/*tile_' num2str(c) '*.tif']);
    for j = 1:length(DDD)
      imfile = [edatadir '/' DDD(j).name];
      outfile = [imfile(1:end-4) '_max.png'];
      fprintf('%s -> %s\n',imfile,outfile);
      %sizeZ and sizeT are swapped in tiles so use modified read function
      [I,metadata] = bfopen_array_t(imfile);
      K = max(I,[],3);
      imwrite(K,outfile,'png');
    end
  end
end



%
% generate isotropic scaled versions of each original channel
% as well as max projections
%
isoscale = 0.2277; %xy down scaling to match z-resolution
DD = dir([basedir '/op*']);
for i = 1:length(DD)
  subdir = DD(i).name;
  basefile = subdir;

  scfile{1} = sprintf('%s_stitch_c0.ome.tif',basefile);
  scfile{2} = sprintf('%s_stitch_c1.ome.tif',basefile);
  scfile{3} = sprintf('%s_stitch_c2.ome.tif',basefile);

  for c = 1:3
    imfile = [basedir '/' subdir '/' scfile{c}];
    outfile = [imfile(1:end-8) '_iso.ome.tif'];
    outfile2 = [imfile(1:end-8) '_max.png'];
    fprintf('%s -> %s\n',imfile,outfile);
    fprintf('%s -> %s\n',imfile,outfile2);

    [I,omeMeta] = bfopen_array(imfile);
    sizeX = omeMeta.getPixelsSizeX(0).getValue(); % image width, pixels
    sizeY = omeMeta.getPixelsSizeY(0).getValue(); % image height, pixels
    sizeZ = omeMeta.getPixelsSizeZ(0).getValue(); % number of Z slices
    sizeC = omeMeta.getPixelsSizeC(0).getValue(); % number of channels
    sizeT = omeMeta.getPixelsSizeT(0).getValue(); % number of time points
    fprintf(1,'Loaded %d x %d x %d with %d channels\n',sizeY,sizeX,sizeZ,sizeC);

    % get the new dimensions
    L = imresize(I(:,:,1,1),isoscale);
    K = zeros(size(L,1),size(L,2),sizeZ,'uint16');
    for z = 1:sizeZ
      K(:,:,z) = uint16(65535*imresize(I(:,:,z),isoscale));
      fprintf('\r  %d: %d / %d   ',c,z,sizeZ);
    end

    metadata = createMinimalOMEXMLMetadata(K);
    % stitching doesn't clone metadata? hmmm.
    %metadata = clone_ome_metadata(metadata,omeMeta);
    
    bfsave(K,outfile,'BigTiff',true,'metadata',metadata);

    imwrite(max(I,[],3),outfile2,'png');

    clear I K;
  end
end


