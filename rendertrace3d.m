function rendertrace3d(trace)

plot3(trace.x(:,1),trace.x(:,2),trace.zscale*trace.x(:,3),'.','MarkerSize',1);
axis image; axis vis3d; grid on;
title(trace.name);
drawnow;

